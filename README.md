# Python Gif Maker
Jupyter notebook with code snippets for generating a gif image from provided pictures.

## List of files
1. gif-maker.ipynb - main Jupyter notebook
2. images/demo - folder with a sample collection of pictures

## Required libs
1. matplotlib
2. opencv
3. imageio